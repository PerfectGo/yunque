package www.larkmidtable.com.reader.sqlserverreader;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import www.larkmidtable.com.channel.Channel;
import www.larkmidtable.com.element.Column;
import www.larkmidtable.com.element.Transformer;
import www.larkmidtable.com.log.LogRecord;
import www.larkmidtable.com.reader.AbstractDBReader;
import www.larkmidtable.com.util.DBType;
import www.larkmidtable.com.util.DBUtil;

import java.sql.*;
import java.util.*;

/**
 * @author fei
 * @description: sqlserver 读工具
 * @date 2022-11-15
 */
public class SqlServerReader extends AbstractDBReader {
	private Connection connection;
	private static Logger logger = LoggerFactory.getLogger(SqlServerReader.class);

	@Override
	public void open() {
		LogRecord logRecord = LogRecord.newInstance();
        logRecord.start("SqlServer的Reader建立连接");
		try {
			connection = DBUtil.getConnection(DBType.SQLSERVER.getDriverClass(), configBean.getUrl(),
					configBean.getUsername(), configBean.getPassword());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			logRecord.end();
		}
	}

	@Override
	public Queue<List<String>> startRead(String[] inputSplits) {
		return null;
	}

	@Override
	public Queue<List<String>> startRead(String inputSplit) {
		LogRecord logRecord = LogRecord.newInstance();
        logRecord.start("SqlServer读取数据操作");
		try {
			logger.info("执行的SQL:"+inputSplit);
			defaultSingleStartRead(connection, inputSplit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logRecord.end();
		return Channel.getQueue();
	}

	@Override
	public void defaultSingleStartRead(Connection connection, String inputSplit) throws Exception {
		List<String> records = new ArrayList<>();
		PreparedStatement preparedStatement = connection.prepareStatement(inputSplit);
		ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			Map<String, Object> map = new HashMap<>();
			ResultSetMetaData metaData = resultSet.getMetaData();
			for (int i = 1; i <= metaData.getColumnCount(); i++) {
				Column column = Transformer.buildColumn(resultSet, metaData, i);
				map.put(metaData.getColumnName(i), column);
			}
			records.add(JSONObject.toJSONString(map));
		}
		logger.info("添加到队列的记录条数{}", records.size());
		// 责任链模式执行数据清洗/转换
		if (records.size() != 0) {
			Channel.getQueue().add(records);
		}
	}

	@Override
	public List<String> defaultInputSplits(String column, String originInput) {
		// 实现SQLSEVER的分页
		List<String> splits = new ArrayList<>();
		int count = count();
		if (count > 0) {
			// 线程数
			int size = this.getConfigBean().getThread();
			// 页大小
			Integer limitSize = count / configBean.getThread();
			Integer lastLimit = Integer.MAX_VALUE;
			for (int i = 0; i < size; i++) {
				boolean lastPage = i == size - 1;
				if (lastPage) {
					lastLimit = limitSize + count % configBean.getThread();
				}
				// 页大小
				int pageSize = lastPage ? lastLimit : limitSize;
				// 偏移量
				int offset = i * limitSize;

				StringBuilder builder = new StringBuilder("SELECT " + column + " FROM (");
				builder.append(originInput).append(") t ").append(" order by ").append(configBean.getSplitPk())
						.append(" offset ").append(offset).append(" rows fetch next ").append(pageSize)
						.append(" rows only");
				splits.add(builder.toString());
			}
		} else {
			splits.add(originInput);
		}
		return splits;
	}

	@Override
	public String[] createInputSplits(int count,int bcount){
		logger.info("SqlServer的Reader开始进行分片开始....");
		String inputSql = String.format("select %s from %s", configBean.getColumn(), configBean.getTable());
		List<String> results = defaultInputSplits(configBean.getColumn(), inputSql);
		logger.info("SqlServer的Reader开始进行分片结束....");
		String[] array = new String[results.size()];
		return results.toArray(array);
	}

	@Override
	public void close() {
		logger.info("SqlServer的Reader开始进行关闭连接开始....");
		DBUtil.close(connection);
		logger.info("SqlServer的Reader开始进行关闭连接结束....");
	}

	@Override
	protected AbstractDbReadTask newDbReadTaskInstance() {
		return null; // TODO
	}

	@Override
	public int count() {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement("SELECT count(*) FROM " + configBean.getTable());
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return resultSet.getInt(1);
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			DBUtil.close(preparedStatement);
		}
		return 0;
	}

}
