package www.larkmidtable.com.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class DbTaskResult extends TaskResult{
    private int successRowNum;
    private int failRowNum;

    public void ok(){
        setSuccess(true);
        setFailRowNum(0);
    }

    public void bad(int failNum){
        setSuccess(false);
        setFailRowNum(failNum);
    }
}
