package www.larkmidtable.com.element;


public interface Record {

    // 默认的添加字段的方法
    public void addColumn(Column column);

    public void setColumn(int i, final Column column);

    public Column getColumn(int i);

    Column getColumn(String colName);

    public String toString();

    public int getColumnNumber();

    public int getByteSize();

    public int getMemorySize();
}
