import com.larkmidtable.yunque.config.ConfigConstant;
import com.larkmidtable.yunque.rpc.ClientServiceProxyFactory;
import com.larkmidtable.yunque.rpc.ServerService;
import org.apache.commons.cli.ParseException;
import org.junit.Test;

/**
 * @author Qing
 * @create 2023-10-15 17:10
 */
public class ClientServiceProxyFactoryTest {

    private  ServerService clientService =null ;
    @Test
    public void test(){


        ServerService clientService = (ServerService) ClientServiceProxyFactory.getClientService(ServerService.class,"127.0.0.1:12345");
        try {
            clientService.executeSQL(new String[]{"string1", "string2","string3"}, 2 ,3 );
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
